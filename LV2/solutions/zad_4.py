import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0) 
die = np.random.randint(low=1, high=7, size=100)
print(die)
plt.hist(die, bins=20)