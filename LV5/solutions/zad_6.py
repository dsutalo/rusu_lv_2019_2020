from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skc
import copy

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)

def kmeans(x,broj_klastera):
    C_x = np.random.randint(np.min(x), np.max(x), size=broj_klastera)
    C_y = np.random.randint(np.min(x), np.max(x), size=broj_klastera)
    C = np.array(list(zip(C_x, C_y)), dtype=np.float32)
    C_old = np.zeros(C.shape)
    clusters = np.zeros(len(x))
    error = dist(C, C_old, None)
    while error != 0:
        for i in range(len(x)):
            distances = dist(x[i], C)
            cluster = np.argmin(distances)
            clusters[i] = cluster
            C_old = copy.deepcopy(C)
            for i in range(broj_klastera):
                points = [x[j] for j in range(len(x)) if clusters[j] == i]
                C[i] = np.mean(points, axis=0)
                error = dist(C, C_old, None)
       
    return clusters

data=generate_data(500,1)
k_means=kmeans(data,3)
plt.scatter(data[:,0],data[:,1],c=k_means)
print(k_means)