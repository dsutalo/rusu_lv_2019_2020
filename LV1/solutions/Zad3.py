import sys

unos = 0

brojevi = []

while(1):
    unos = input("Unesite broj")
    
    if unos == "Done":
        print(unos)
        break   
    
    try: 
        brojevi.append(float(unos))
        
    except ValueError : 
        print("Niste unijeli broj")
        
print("Velicina liste: ", len(brojevi))
print("Minimum liste: ", min(brojevi))
print("Maximum liste: ", max(brojevi))
print("Prosijek liste: ", sum(brojevi)/len(brojevi))