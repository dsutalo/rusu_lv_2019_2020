import sys

grade = input("Enter grade in range 0.0 - 1.0 ")

try :
    grade = float(grade)
except ValueError: 
    print("You didn't enter a number")
    sys.exit()
    
if(grade >= 0.0 and grade <= 1.0):
    if grade >= 0.9:
        print("A")
    elif grade >= 0.8:
        print("B")
    elif grade >= 0.7:
        print("C")
    elif grade >= 0.6:
        print("D")
    elif grade < 0.6:
        print("F")
        
else:
    print("Grade is not in range 0.0 and 1.0")