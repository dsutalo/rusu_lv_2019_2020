import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
# print(len(mtcars))
# print(mtcars)

# 1. zad
# mtcars1 = mtcars.groupby('cyl').mean()
# print(mtcars1)
# plt.bar([4, 6, 8], mtcars1['mpg'], align='center', alpha= 0.5)
# plt.show()

# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
# mtcars2 = mtcars[['cyl', 'wt']]
# mtcars2.boxplot(by='cyl')
# plt.show()

# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
# potrošnju od automobila s automatskim mjenjačem?

# mtcars3 = mtcars.groupby('am').mean()
# print(mtcars3)
# plt.bar([0, 1], mtcars3['mpg'], align='center', alpha=1, linewidth = 0.1)
# plt.show()

# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#    mjenjačem.

# plt.scatter(mtcars[mtcars.am == 0]['qsec'], mtcars[mtcars.am == 0]['hp'], label='automatic', s=mtcars[mtcars.am == 0]['wt']*20)
# # plt.scatter(mtcars[mtcars.am == 1]['qsec'], mtcars[mtcars.am == 1]['hp'], label='manual', s=mtcars[mtcars.am == 1]['wt']*20)
# # plt.legend(loc=1)
# # plt.show()

print(mtcars.tail(4))