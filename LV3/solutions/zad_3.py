from urllib.request import urlopen
#import urllib.request 
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np
#import matplotlib.pyplot as plt 
#import numpy as np

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=01.01.2018'

airQualityHR = urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

#3.2)

najvece_vrijednosti=df.sort_values(by=["mjerenje"],ascending=True)
print("Dani kad je koncentracija najveća: ",najvece_vrijednosti["vrijeme"].head(3))

#3.3)

mjeseci=np.arange(1,13,1)
izostali=[]#polje za brojanje izostalih vrijednosti na y osi

for i in range(1,13):
    mjesec=df[df.month==i]
    if ((i==1) or (i==3) or(i==5) or(i==7) or (i==8) or (i==10) or (i==12)):#provjeravamo dane za koje nisu unijeti podaci u mjesecima od 31 dan i oduzimamo dane koji nisu unijeti
        izostali.append(31-len(mjesec)) 
    elif (i==4) or (i==6) or(i==9) or(i==11):#isto za mjesece s 30 dana
        izostali.append(30-len(mjesec))
    else:#za mjesec veljacu
        izostali.append(28-len(mjesec))

plt.figure()
plt.bar(mjeseci,izostali)   #mjeseci i broj izostalih po mjesecu se ispituju
plt.title("Broj izostalih vrijednosti za svaki mjesec")
plt.xlabel("mjesec")
plt.ylabel("Broj dana kad se nije mjerilo")
plt.grid(linestyle="--",linewidth=2)

#3.4)

kolovoz=df[df.month==8]#podaci za mjesec kolovoz i prosinac
prosinac=df[df.month==12]
koncentracija_8mj=[]#polja u koja cemo spremiti koncentracije tijekom kolovoza i prosinca
koncentracija_12mj=[]

for i in kolovoz["mjerenje"]:#spremanje koncentracija u posebna polja
    koncentracija_8mj.append(i)

for i in prosinac["mjerenje"]:
    koncentracija_12mj.append(i)

plt.figure()
plt.boxplot([koncentracija_8mj,koncentracija_12mj],positions=[8,12],widths=1)#plotanje vrijednosti koncentracija za 8 i 12 mjesec
plt.title("Koncentracija PM10 tijekom 12. i 8. mjeseca")
plt.xlabel("Mjeseci")
plt.ylabel("Koncentracija PM10(micrograma/m3)")
plt.grid(linestyle="-",linewidth=2)
plt.legend([kolovoz,prosinac])

#3.5)

radni_dan=df[(df.dayOfweek==0) | (df.dayOfweek==1) | (df.dayOfweek==2) | (df.dayOfweek==3) | (df.dayOfweek==4)]#spremanje u novu strukturu gdje su dani u tjednu od 0.(pon) do 4.(pet)
vikend=df[(df.dayOfweek==5) | (df.dayOfweek==6)]#subota i nedjelja
koncentracija_radni=[]#polja za vrijednosti koncentracija
koncentracija_vikend=[]

for i in radni_dan["mjerenje"]:
    koncentracija_radni.append(i)
for i in vikend["mjerenje"]:
    koncentracija_vikend.append(i)

plt.figure()
plt.hist([koncentracija_radni,koncentracija_vikend],bins=20,color=["cyan","magenta"])
plt.title("Koncentracija PM10 radnim danima i  vikendom")
plt.xlabel("Dani u tjednu")
plt.ylabel("Koncentracija PM10(microg/m3)")
plt.grid(linestyle="-",linewidth=2)
plt.legend(["Radni dani","Vikend"],loc="Upper right")














